package movieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac046 on 2017/5/15.
  */
object TopRatingsApp extends App {
  val conf = new SparkConf().setAppName("TopRatings")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val ratings: RDD[(Int,Double)]= sc.textFile("/Users/mac046/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
      .filter(strs=>strs(1)!="movieId")
    .map(strs=>{
      //println(strs.mkString(","))//**如果沒註解掉，會一直跑這邊，而印出很多
      (strs(1).toInt,strs(2).toDouble)
    })

  //ratings.take(5).foreach(println)

  //val totalRatingBtMovieId=ratings.reduceByKey((acc,curr)=>acc+curr)
  //totalRatingBtMovieId.takeSample(false,5).foreach(println)

  val averageRatingBymovieId:RDD[(Int,Double)]=ratings.mapValues(v=> v -> 1)
    .reduceByKey((acc,curr)=>{
      (acc._1+curr._1)->(acc._2+curr._2)
    }).mapValues(kv=>kv._1/kv._2.toDouble)

  //val top10=averageRatingBymovieId.sortBy(_._2,false).take(10)
  //top10.foreach(println)
  //averageRatingBymovieId.takeSample(false,5).foreach(println)

  val movies: RDD[(Int,String)]=sc.textFile("/Users/mac046/Downloads/ml-20m/movies.csv")
      .map(str=>str.split(","))  //字串用，分割
        .filter(strs=>strs(0)!="movieId")
      .map(strs=>(strs(0).toInt,strs(1)))

  val joined:RDD[(Int,(Double,String))]=averageRatingBymovieId.join(movies)
//Int：MovieId(檔案裡的名稱)，Double：平均，String：電影名稱
  joined.map(v=>v._1+","+v._2._2+","+v._2._1).saveAsTextFile("result")

  val res:RDD[((Int,String),Double]=sc.textFile("result").map(str=>str.split(","))
    .map(strs=>strs(0).toInt->strs(1)->strs(2).toDouble)

  //joined.takeSample(false,5).foreach(println)



}
