import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac046 on 2017/5/8.
  */
object WordCountApp extends App {

  val conf = new SparkConf().setAppName("WordCount")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val lines=sc.textFile("/Users/mac046/Downloads/spark-doc.txt")
  //lines.take(10).foreach(println)
  //lines.map(str=>str.split(" ").toList.take(10).foreach(println))
  //lines.flatMap(str=>str.split(" ").toList).take(10).foreach(println)
  val words=lines.flatMap(i=>i.split(" "))

  words.map(_->1).reduceByKey(_+_).take(10).foreach(println)
  //words.groupBy(str=>str).mapValues(_.size).take(10).foreach(println)
  //words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr).take(10).foreach(println)
  readLine()

}
