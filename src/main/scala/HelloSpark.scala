import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac046 on 2017/5/1.
  */
object HelloSpark extends App {
  val conf = new SparkConf().setAppName("HelloSpark")
    .setMaster("local[*]")

  val sc=new SparkContext(conf)
  //List(
  val nums: RDD[String]=sc.textFile("numbs.txt")
  //nums.foreach(str=>{
    //println("==============")
    //println(str)
  //})
  //readLine()
  val range=1 to 100

  val intRdd: RDD[Int]=sc.parallelize(range)
  println("range:"+range)
  println("rdd:"+intRdd)
  //println(intRdd.sum())
  //println(nums.count())
  //readLine()
}
