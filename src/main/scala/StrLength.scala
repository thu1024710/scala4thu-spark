import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac046 on 2017/5/1.
  */
object StrLength extends App {
  val conf= new SparkConf().setAppName("StrLength")
    .setMaster("local[*]")
  val sc= new SparkContext(conf)

  val nums=sc.textFile("numbs.txt")
  val strLength:RDD[Int]=nums.map(_.length)
  println(strLength.collect().toList)
}
